#!/bin/bash



qtdlinhas1=$(wc -l < "$1" 2>/dev/null)
qtdlinhas2=$(wc -l < "$2" 2>/dev/null)
qtdlinhas3=$(wc -l < "$3" 2>/dev/null)
qtdlinhas4=$(wc -l < "$4" 2>/dev/null)

linhasmaior=$qtdlinhas1
arquivomaiorlinhas=$1

[ $qtdlinhas2 -gt $linhasmaior ] && { linhasmaior=$qtdlinhas2; arquivomaiorlinhas=$2; }
[ $qtdlinhas3 -gt $linhasmaior ] && { linhasmaior=$qtdlinhas3; arquivomaiorlinhas=$3; }
[ $qtdlinhas4 -gt $linhasmaior ] && { linhasmaior=$qtdlinhas4; arquivomaiorlinhas=$4; }

echo -e "Arquivo com maior número de quantidade de linhas é: $arquivomaiorlinhas\n"

[ -n "$arquivomaiorlinhas" ] && [ -f "$arquivomaiorlinhas" ] && { echo -e "Exibindo abaixo o conteúdo com maior quantidade de linhas: \n"; cat "$arquivomaiorlinhas"; }

