#!/bin/bash

a1=$1
a2=$2
a3=$3
a4=$4

qtdlinhas=($(wc -l "$a1" "$a2" "$a3" "$a4" 2>/dev/null))
linhasmaior=${qtdlinhas[0]}
arquivomaiorlinhas=$a1

((qtdlinhas[1] > linhasmaior)) && { linhasmaior=${qtdlinhas[1]}; arquivomaiorlinhas=$a2; }
((qtdlinhas[2] > linhasmaior)) && { linhasmaior=${qtdlinhas[2]}; arquivomaiorlinhas=$a3; }
((qtdlinhas[3] > linhasmaior)) && { linhasmaior=${qtdlinhas[3]}; arquivomaiorlinhas=$a4; }

echo -e "Arquivo com maior número de quantidade de linhas é: $arquivomaiorlinhas\n"
echo -e "Exibindo abaixo o conteúdo com maior quantidade de linhas: \n"
[ -n "$arquivomaiorlinhas" ] && cat "$arquivomaiorlinhas"
