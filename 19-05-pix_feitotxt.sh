#!/bin/bash

if [ -e /tmp/$1 ]; then
	echo "xico enviou pix em /tmp/pix_feito.txt"
	cat $1
elif [ -e /home/ifpb/$1 ]; then
	echo "xico enviou pix /home/ifpb/pix_feito.txt"
	cat $1
elif [ -e /proc/$1 ]; then
	echo "xico enviou pix /proc/pix_feito.txt"
	cat $1
elif [ -e /var/log/$1 ]; then
	echo "xico enviou pix /var/log/pix_feito.txt"
	cat $1
elif [ -e ./$1 ]; then
	echo "xico enviou pix ./pix_feito.txt"
	cat $1
else
	echo "xico se atrapalhou e não enviou pix"
fi
