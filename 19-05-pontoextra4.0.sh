#!/bin/bash

qtdlinhas1=$(wc -l < "$1" 2>/dev/null)
qtdlinhas2=$(wc -l < "$2" 2>/dev/null)
qtdlinhas3=$(wc -l < "$3" 2>/dev/null)
qtdlinhas4=$(wc -l < "$4" 2>/dev/null)
arquivomaiorlinhas="$qtdlinhas1"
nomearquivo="$1"

if [ $qtdlinhas2 -gt $qtdlinhas1 ]; then
	arquivomaiorlinhas="$qtdlinhas2"
	nomearquivo="$2"
fi

if [ $qtdlinhas3 -gt $qtdlinhas2 ]; then
	arquivomaiorlinhas="$qtdlinhas3"
	nomearquivo="$3"
fi

if [ $qtdlinhas4 -gt $qtdlinhas3 ]; then
	arquivomaiorlinhas="$qtdlinhas4"
	nomearquivo="$4"
fi

echo -e "Exibindo o arquivo  com maior quantidade de linhas $nomearquivo abaixo exibindo seu conteúdo: \n"
cat $nomearquivo
